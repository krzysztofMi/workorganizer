module org.workflow {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml.bind;

    opens org.workflow to javafx.fxml;
    opens org.objects to java.xml.bind;
    exports org.workflow;
}