package org.objects;




import javax.xml.bind.annotation.*;
import java.util.ArrayList;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Project{

    @XmlAttribute(name = "id")
    private int id;
    private String name;
    private String type;
    private String description;
    @XmlElementWrapper(name = "tasks")
    @XmlElement(name = "task")
    private ArrayList<String> toDoList;

    public Project(){
        toDoList = new ArrayList<>();
    }
    public void setName(String name){
        this.name = name;
    }
    public void setType(String type){
        this.type = type;
    }
    public void setDescription(String descritpion){
        this.description = descritpion;
    }
    public void setId(int id){this.id = id;}
    public void addTaskToDo(String task){ toDoList.add(task);}
    public int getId(){return id;}
    public String getName(){return name;}
    public String getType(){ return type;}
    public String getDescription(){ return description;}
    public ArrayList<String> getToDoList(){ return toDoList;}

    @Override
    public String toString() {
        return name + " " + type + "\n" + description;
    }


}
