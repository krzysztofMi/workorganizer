package org.objects;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class XmlFile {
    public File file;

    public XmlFile(String path) throws IOException{
        file = new File(path);
        isFile();
    }

    public boolean isFile() throws IOException {
        return file.createNewFile();
    }

    public void attachProjectToXml(Project project) throws JAXBException {
        Projects projects = loadFromXml();
        projects.addProject(project);
        List<Project> projectList = projects.getProjects();
        for(int i = 0; i<projectList.size(); i++){
            projectList.get(i).setId(i+1);
        }
        saveToXml(projects);
    }

    public Projects loadFromXml() throws  JAXBException{
        if(!isEmpty()){
            JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (Projects) unmarshaller.unmarshal(file);
        }
        return null;
    }

    public void saveToXml(Projects projects) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(projects, file);
    }

    public boolean isEmpty(){
        return file.length() == 0;
    }
}
