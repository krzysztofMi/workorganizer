package org.utils;

import javafx.scene.control.Alert;

public class UtilMethods {

    public static void alertWindow(String title, String information){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(information);
        alert.showAndWait();
    }

}
