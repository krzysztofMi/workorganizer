package org.workflow;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.objects.Project;
import org.objects.Projects;
import org.objects.XmlFile;

public class DeleteProjectController {
    @FXML
    Label deleteLabel;

    String pathToData;
    Project projectToRemove;
    MainController controller;
    @FXML
    void initialize(){
        deleteLabel.setText("Do you want delete\n selected project?");
        deleteLabel.setAlignment(Pos.CENTER);
    }

    public void yesAction() throws  Exception {
        var file = new XmlFile(pathToData);
        Projects projects = file.loadFromXml();
        Project tmp = null;
        for(Project project: projects.getProjects()){
            if(project.getName().equals(projectToRemove.getName())){
                tmp = project;
            }
        }
        projects.getProjects().remove(tmp);
        file.saveToXml(projects);
        controller.initializeTreeProject();
        noAction();
    }

    public void noAction() {
        Stage stage = (Stage)deleteLabel.getScene().getWindow();
        stage.close();
    }

    public void setProjectToRemove(Project project){
        projectToRemove = project;
    }
    public void setPathToData(String pathToData){
        this.pathToData = pathToData;
    }
    public void setController(MainController controller){
        this.controller = controller;
    }
}
