package org.workflow;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import org.objects.Project;
import org.objects.Projects;
import org.objects.XmlFile;

import java.util.List;

public class RemoveTaskController {
    @FXML
    ComboBox<String> taskName;
    List<String> tasks;
    Project selectedProject;
    MainController controller;
    String pathToData;

    public void setTasks(List<String> tasks){
        this.tasks = tasks;
        taskName.getItems().addAll(tasks);
    }

    public void setProject(Project project){selectedProject = project; }
    public void setController(MainController controller){this.controller = controller; }
    public void setPathToData(String path){this.pathToData = path; }

    @FXML
    public void cancel(){
        var stage = (Stage) taskName.getScene().getWindow();
        stage.close();
    }
    @FXML
    public void remove() throws Exception{
        var file = new XmlFile(pathToData);
        Projects projects = file.loadFromXml();
        Project tmp = null;
        for(Project project: projects.getProjects()){
            if(selectedProject.getName().equals(project.getName())){
               tmp = project;
            }
        }
        tmp.getToDoList().remove(taskName.getValue());
        file.saveToXml(projects);
        controller.initializeTaskList();
        cancel();
    }
}
