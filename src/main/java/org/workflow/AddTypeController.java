package org.workflow;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class AddTypeController {
    @FXML
    TextField typeName;

    MainController controller;

    @FXML
    public void addType() throws IOException, Exception {
        BufferedWriter writer = new BufferedWriter(
                new FileWriter("src/main/resources/org/workflow/types.txt", true)
        );
        writer.write(typeName.getText());
        writer.newLine();
        writer.close();
        cancelAdding();
        controller.loadTypes();
        controller.initializeTreeProject();
    }

    @FXML
    public  void cancelAdding(){
        Stage stage = (Stage)typeName.getScene().getWindow();
        stage.close();
    }

    public void setMainController(MainController controller){
        this.controller = controller;
    }
}
