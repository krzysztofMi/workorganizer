package org.workflow;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.objects.Project;

import java.io.IOException;
import java.util.List;

public class AddProjectController {
    @FXML
    TextField projectName;
    @FXML
    ComboBox<String> typeSelector;
    @FXML
    Button cancelButton;

    private List<String> types;
    private Project project;
    private MainController controller;

    public AddProjectController(){

    }
    @FXML
    void initialize(){
    }

    @FXML
    public void nextWindow() throws IOException {
        if(projectName.getText().trim().isEmpty()){
            projectName.setPromptText("Please name your project.");
            return;
        }else if(typeSelector.getValue() == null){
            typeSelector.setPromptText("Choose type!");
            return;
        }
        project = new Project();
        project.setName(projectName.getText());
        project.setType(typeSelector.getValue());
        var loader = App.loadFXML("addProject2");
        Pane pane = loader.load();
        AddProjectController2 nextController = loader.getController();
        nextController.setProject(project);
        nextController.setMainController(controller);
        nextController.setTypes(types);
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.setScene(new Scene(pane));
    }

    @FXML
    public void cancelAdding(){
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.close();
    }


    public void setMainController(MainController controller){
        this.controller = controller;
    }
    public void setTypes(List<String> types){
        this.types = types;
        typeSelector.getItems().addAll(types);
    }
}
