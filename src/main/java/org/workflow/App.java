package org.workflow;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("mainScene").load());
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));
        scene.getStylesheets().add(this.getClass().getResource("theme.css").toString());
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }


    public static FXMLLoader loadFXML(String fxml) throws IOException {
        var fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader;
    }

    public static void addStage(Object parent, String title) throws IOException{
        var scene = new Scene((Parent)parent);
        var stage = new Stage();
        stage.setScene(scene);
        stage.setTitle(title);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}