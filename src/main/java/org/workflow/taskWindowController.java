package org.workflow;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.objects.Project;
import org.objects.XmlFile;

public class taskWindowController {

    @FXML
    Button cancelButton;
    @FXML
    TextField taskText;

    private Project project;
    private MainController controller;

    @FXML
    public void cancelAdding(){
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void addTask() throws Exception {
        var task = taskText.getText();
        if(task.trim().isEmpty()){
            taskText.setText("");
            taskText.setPromptText("Can't accept empty task!");
            return;
        }
        XmlFile file = new XmlFile("src/main/resources/org/workflow/data.xml");
        var projects = file.loadFromXml();
        for(Project proj: projects.getProjects()){
            if(proj.getId() == project.getId()){
                proj.addTaskToDo(task);
            }
        }
        file.saveToXml(projects);
        controller.initializeTaskList();
        cancelAdding();
    }

    public void setProject(Project project){
        this.project = project;
    }
    public void setMainController(MainController controller){
        this.controller = controller;
    }
}
