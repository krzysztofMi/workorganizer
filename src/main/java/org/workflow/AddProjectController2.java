package org.workflow;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.objects.Project;
import org.objects.Projects;
import org.objects.XmlFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddProjectController2{

    @FXML
    Button cancelButton;
    @FXML
    TextArea projectDescription;
    private Project project;
    private List<String> types;
    private MainController controller;

    public void setProject(Project project){
        this.project = project;
    }

    @FXML
    public void cancelAdding(){
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void returnToPreviousWindow() throws IOException {
        var loader = App.loadFXML("addProject1");
        Pane pane = loader.load();
        AddProjectController mainController = loader.getController();
        mainController.setMainController(controller);
        mainController.setTypes(types);
        Stage stage = (Stage)cancelButton.getScene().getWindow();
        stage.setScene(new Scene(pane));
    }

    @FXML
    public void addProject() throws Exception{
        project.setDescription(projectDescription.getText());
        XmlFile file = new XmlFile("src/main/resources/org/workflow/data.xml");
        if(file.isFile() || file.isEmpty()){
            Projects projects = new Projects();
            List<Project> projectsList = new ArrayList<>();
            project.setId(1);
            projectsList.add(project);
            projects.setProjects(projectsList);
            file.saveToXml(projects);
        }else{
            file.attachProjectToXml(project);
        }
        cancelAdding();
        controller.initializeTreeProject();
    }

    public void setMainController(MainController controller){
        this.controller = controller;
    }
    public void setTypes(List<String> types){ this.types = types; }
}

