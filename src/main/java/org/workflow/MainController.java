package org.workflow;


import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import org.objects.Project;
import org.objects.Projects;
import org.objects.XmlFile;
import org.utils.UtilMethods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainController {

    @FXML
    TreeView projectTree;
    @FXML
    TextArea descriptionField;
    @FXML
    ToggleButton editButton;
    @FXML
    VBox taskList;

    Projects projects;
    Project selectedProject;
    private List<String> types;
    final private String pathToData = "src/main/resources/org/workflow/data.xml";


    public List<String> getTypes(){
        return types;
    }

    public MainController() throws Exception{
        selectedProject = null;
        types = new ArrayList<>();

        loadTypes();
    }

    public void initialize() throws Exception{
        initializeTreeProject();
    }

    @FXML
    public void addProject() throws IOException {
        if(types.isEmpty()){
            UtilMethods.alertWindow("No types!", "No types in program" +
                    " add type to add new project.");
            return;
        }
        var loader = App.loadFXML("addProject1");
        var container = loader.load();
        AddProjectController controller = loader.getController();
        controller.setMainController(this);
        controller.setTypes(types);
        App.addStage(container, "Add project.");
    }


    public void loadTypes() throws Exception {
        types.clear();
        var file = new File("src/main/resources/org/workflow/types.txt");
        if(!file.exists()){
            file.createNewFile();
        }
        var in = new BufferedReader(new FileReader(file));
        String str;
        while ( (str = in.readLine()) != null){
            types.add(str);
        }
        in.close();
    }


    public void mouseClick(MouseEvent event)throws Exception{
        setSelectedProject();
        if(selectedProject!=null) {
            descriptionField.setText(selectedProject.getDescription());
            initializeTaskList();
        }

        if(event.getButton().equals(MouseButton.SECONDARY)){

        }
    }

    private String getLeadSelect(){
        var selectedItem = (TreeItem<String>)projectTree.getSelectionModel().getSelectedItem();
        return selectedItem == null ? null : selectedItem.getValue();
    }

    private void setSelectedProject(){
        String name = getLeadSelect();
        if(name!=null) {
            for (Project project : projects.getProjects()) {
                if (name.equals(project.getName())) {
                    selectedProject = project;
                }
            }
        }
    }

    @FXML
    public void editDescription(){
        if(selectedProject == null){
            editButton.setSelected(false);
            return;
        }
        if(editButton.isSelected()) {
            descriptionField.setEditable(true);
        }else{
            selectedProject.setDescription(descriptionField.getText());
            descriptionField.setEditable(false);
            try{
                XmlFile file = new XmlFile(pathToData);
                file.saveToXml(projects);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    @FXML
    public void addTask() throws IOException{
        if(selectedProject!=null) {
            var loader = App.loadFXML("taskWindow");
            var container = loader.load();
            taskWindowController controller = loader.getController();
            controller.setProject(selectedProject);
            controller.setMainController(this);
            App.addStage(container, "Add task.");
        }
    }

    public void initializeTreeProject() throws  Exception{
        projects = new XmlFile(pathToData).loadFromXml();
        if(projects != null) {
            projectTree.setRoot(new TreeItem<String>("Projects"));
            for (String type : types) {
                var item = new TreeItem<>(type);
                for (Project project : projects.getProjects()) {
                    if(type.equals(project.getType())){
                        var subItem = new TreeItem<>(project.getName());
                        item.getChildren().add(subItem);
                    }
                }
                projectTree.getRoot().getChildren().add(item);
            }
        }
    }
    public void initializeTaskList() throws Exception{
        projects = new XmlFile(pathToData).loadFromXml();
        setSelectedProject();
        var toDoList = selectedProject.getToDoList();
        taskList.getChildren().clear();
        for(String task: toDoList){
            var box = new CheckBox(task);
            box.setTextAlignment(TextAlignment.LEFT);
            box.setMinSize(50, 25);
            taskList.getChildren().add(box);
        }
        var addButton = new Button("+");
        addButton.setMinWidth(29);
        addButton.setMinHeight(17);
        addButton.setOnAction(e-> {
            try {
                addTask();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        taskList.getChildren().add(addButton);
    }

    @FXML
    public void removeTask() throws Exception{
        if(taskList.getChildren().size()<2){
            return;
        }
        var loader = App.loadFXML("removeTaskWindow");
        var container = loader.load();
        RemoveTaskController controller = loader.getController();
        List<String> tasks = new ArrayList<>();
        for(Node node: taskList.getChildren()){
            if(node instanceof CheckBox){
                tasks.add(((CheckBox) node).getText());
            }
        }
        controller.setTasks(tasks);
        controller.setProject(selectedProject);
        controller.setController(this);
        controller.setPathToData(pathToData);
        App.addStage(container, "Remove task.");
    }
    @FXML
    public void removeProject() throws  Exception {
        if(selectedProject == null){
            UtilMethods.alertWindow("Not selected.", "No project selected" +
                    " for remove");
            return;
        }
        var loader = App.loadFXML("deleteWindow");
        var container = loader.load();
        DeleteProjectController controller = loader.getController();
        controller.setPathToData(pathToData);
        controller.setProjectToRemove(selectedProject);
        controller.setController(this);
        App.addStage(container, "Remove project.");
    }
    @FXML
    public  void exit(){
        Platform.exit();
    }
    @FXML
    public void addType() throws IOException{
        var loader = App.loadFXML("addType");
        var container = loader.load();
        AddTypeController controller = loader.getController();
        controller.setMainController(this);
        App.addStage(container, "Add project.");
    }
    @FXML
    public void removeType() throws  Exception{
        if(types.isEmpty()){
            UtilMethods.alertWindow("No types.", "No types to remove.");
            return;
        }
        var loader = App.loadFXML("deleteTypeWindow");
        var container = loader.load();
        DeleteTypeController controller = loader.getController();
        controller.setTypes(types);
        controller.setMainController(this);
        App.addStage(container, "Remove type.");
    }

}