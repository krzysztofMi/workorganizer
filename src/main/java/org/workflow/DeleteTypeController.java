package org.workflow;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class DeleteTypeController {

   @FXML
   ComboBox<String> typeName;
   List<String> types;
   MainController controller;

   public void setTypes(List<String> types){
       this.types = types;
       typeName.getItems().addAll(types);
   }
   public void setMainController(MainController controller){
       this.controller = controller;
   }


   public void remove() throws Exception{
       types.remove(typeName.getValue());
       saveTypesToFile();
       controller.initializeTreeProject();
       cancel();
   }

   public void cancel(){
       Stage stage = (Stage)typeName.getScene().getWindow();
       stage.close();
   }

    public void saveTypesToFile() throws IOException, Exception {
        BufferedWriter writer = new BufferedWriter(
                new FileWriter("src/main/resources/org/workflow/types.txt", false)
        );
        for(String type: types) {
            writer.write(type);
            writer.newLine();
        }
        writer.close();
    }

}
